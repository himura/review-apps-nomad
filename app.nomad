job "[[.name]]" {
    type = "service"
    datacenters = ["[[.dc]]"]
    update {
        canary = 1
        max_parallel = 1
        health_check = "checks"
        auto_revert = true
        auto_promote = true
    }
    group "[[.name]]" {
        count = 1
        task "[[.name]]" {
            driver = "docker"
            config {
                image = "[[.image]]"
                ports = ["http"]
            }
            resources {
                cpu    = 50 # MHz
                memory = 50 # MB
            }
            service {
                name = "[[.name]]"
                port = "http"
                tags = [
                    "urlprefix-[[.urlprefix]]"
                ]
                check {
                    type = "http"
                    path = "/"
                    interval = "10s"
                    timeout = "1s"
                }
            }
        }
        network {
            port "http" {
                to = 80
            }
        }
    }
}
